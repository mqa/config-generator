#include <sourcemod>
//#include <tf2>
//#include <sdkhooks>
#include <sdktools>
//#include <tf2_stocks>
//#include <smlib>


#define PLUGIN_NAME "mqa_spawner"
#define PLUGIN_VERSION "1.0.0"
public Plugin:myinfo = 
{
	name = PLUGIN_NAME,
	author = "turtsmcgurts & DeuceBox",
	description = "Plugin to read/write spawn locations.",
	version = PLUGIN_VERSION,
	url = "http://steamcommunity.com/id/aadic68/ & http://steamcommunity.com/id/DeuceBox"
}

#define TYPE_DEATHMATCH 0
#define TYPE_ELIMINATION 1
#define TYPE_BBALL 2
#define TYPE_ULTIDUO 3

#define NEW_SPAWN -1 // used for spawn_points
#define MAX_ARENAS 24
#define MAX_MAPS 24
#define MAX_SPAWNS 24
#define RED 0
#define BLU 1

#define READING_MAP_NAMES 0
#define READING_ARENA_NAMES 1
#define READING_OPTION_NAMES 2
#define READING_SHARED_SPAWNS 3
#define READING_RED_SPAWNS 4
#define READING_BLU_SPAWNS 5
#define END_OF_READING_ARENA 6

// for spawn arrays
#define POSITION 0
#define EYES 1

enum Options
{
	red_team_size = 0,
	blu_team_size,
	bool:separate_spawns,
	gametype,
	Float:respawn_delay,
	Float:health,
	bool:unlimited_ammo
}

new Handle:smc_file_handle = INVALID_HANDLE,
	Handle:teleport_handle = INVALID_HANDLE,
	Handle:workaround_timer_handle = INVALID_HANDLE;

new String:config_path[128],
	String:map_names[MAX_MAPS][128],
	String:arena_names[MAX_MAPS][MAX_ARENAS][256],
	String:arena_options[MAX_MAPS][MAX_ARENAS][Options][256],
	parsing_state,
	current_map_index = 0,
	map_index = 0,
	String:temp_value[128],
	user, // person who did !add
	arena_index[MAX_MAPS] = 0,
	String:map_name[128];
	
new new_spawn_amount = 0,
	Float:new_spawn_points[MAX_SPAWNS][2][2][3], // [spawn_index][team][pos/eye][0 1 2]
	cycle_current_spawn,
	bool:first_cycle = true,
	selected_team;

show_spawn_points(client) // display a (player?) sized prop at each location
{
}
add_current_location(client, team) // adds players current location as spawn
{
	new Float:temp_position[3], Float:temp_eyes[3];
	GetClientAbsOrigin(client, Float:temp_position);
	GetClientEyeAngles(client, Float:temp_eyes);
	user = client;
	selected_team = team;
	
	for (new i = 0; i < 3; i++)
	{
		new_spawn_points[new_spawn_amount][team][POSITION][i] = temp_position[i];
		new_spawn_points[new_spawn_amount][team][EYES][i] = temp_eyes[i];
	}
	if(teleport_handle != INVALID_HANDLE)
	{
		KillTimer(teleport_handle);
		teleport_handle = INVALID_HANDLE;
	}
	//teleport_handle = CreateTimer(0.5, timer_teleport, new_spawn_amount);
	
	// cycle_current_spawn = 0;
	// TeleportEntity(client, new_spawn_points[new_spawn_amount][POSITION], new_spawn_points[new_spawn_amount][EYES], NULL_VECTOR);
	
	new_spawn_amount++;
	show_create_spawn_menu(client, -1);
}


public Action:timer_teleport(Handle:timer, any:to_specific_point)
{
	if(to_specific_point != -1) // to only one location
	{
		TeleportEntity(user, new_spawn_points[to_specific_point][selected_team][POSITION], new_spawn_points[to_specific_point][selected_team][EYES], NULL_VECTOR);
		teleport_handle = INVALID_HANDLE;
	}
}


public OnPluginStart()
{	
	RegConsoleCmd("add", Configuration, "");
	RegConsoleCmd("spawn", Add_Spawn, "");
	
}

public Action:Add_Spawn(client, args)
{
	show_create_spawn_menu(client, -1);
}
public Action:Configuration(client, args)
{
	map_index = 0;
	arena_index[map_index] = 0;
	user = client;
	
	BuildPath(Path_SM, config_path, sizeof(config_path), "configs/mqa.cfg");
	Initialize_User_Parser()
	
	new SMCError:err = SMC_ParseFile(smc_file_handle, config_path);
	if (err != SMCError_Okay)
	{
		decl String:buffer[64];
		if (SMC_GetErrorString(err, buffer, sizeof(buffer)))
		{
			PrintToChatAll("%s", buffer);
		} else {
			PrintToChatAll("Fatal parse error");
		}
	}
}
Initialize_User_Parser()
{
	if (smc_file_handle == INVALID_HANDLE)
	{
		smc_file_handle = SMC_CreateParser();
		SMC_SetReaders(smc_file_handle, ReadConfig_NewSection, ReadConfig_KeyValue, ReadConfig_EndSection);
	}
}

public Action:timer_workaround(Handle:timer, any:client)
{
	// show_red_player_amount_menu(client, -1);
}

public SMCResult:ReadConfig_NewSection(Handle:smc, const String:name[], bool:opt_quotes)
{
	if (parsing_state == READING_MAP_NAMES)
	{
		PrintToChatAll("MAP::: %s", name);
		strcopy(map_names[map_index], 256, name)
		/*if (strcmp(map_names[map_index], map_name) == 0)
		{
			current_map_index = map_index;
		}*/
		map_index++;
		parsing_state = READING_ARENA_NAMES;
	}
	else if (parsing_state == READING_ARENA_NAMES)
	{
		strcopy(arena_names[map_index][arena_index[map_index]], 256, name);
		PrintToChatAll("ARENA::: %s [%i][%i]", arena_names[map_index][arena_index[map_index]], map_index, arena_index[map_index]);
		arena_index[map_index]++;
		parsing_state = READING_OPTION_NAMES;
	}
	//else
		//PrintToChatAll("OPTION::: %s", name);
}
public SMCResult:ReadConfig_KeyValue(Handle:smc, const String:key[], const String:value[], bool:key_quotes, bool:value_quotes)
{
	if (parsing_state == READING_OPTION_NAMES)
	{
		if (strcmp(key, "red_team_size") == 0)
			strcopy(arena_options[map_index][arena_index[map_index]][red_team_size], 256, value);
		else if (strcmp(key, "blu_team_size") == 0)
			strcopy(arena_options[map_index][arena_index[map_index]][blu_team_size], 256, value);
		else if (strcmp(key, "separate_spawns") == 0)
			strcopy(arena_options[map_index][arena_index[map_index]][separate_spawns], 256, value);
		else if (strcmp(key, "health") == 0)
			strcopy(arena_options[map_index][arena_index[map_index]][health], 256, value);
		else if (strcmp(key, "unlimited_ammo") == 0)
			strcopy(arena_options[map_index][arena_index[map_index]][unlimited_ammo], 256, value);
		else if (strcmp(key, "respawn_delay") == 0)
			strcopy(arena_options[map_index][arena_index[map_index]][respawn_delay], 256, value);
		//... 
	}
	//else if (parsing_state == READING_RED_SPAWNS
}
public SMCResult:ReadConfig_EndSection(Handle:smc)
{
	if (parsing_state == READING_OPTION_NAMES)
	{
		PrintToChatAll("1");
		if (strcmp(arena_options[map_index][arena_index[map_index]][red_team_size], arena_options[map_index][arena_index[map_index]][blu_team_size]) == 0 &&
		    strcmp(arena_options[map_index][arena_index[map_index]][separate_spawns], "false") == 0)
			parsing_state = READING_SHARED_SPAWNS;
		else
			parsing_state = READING_RED_SPAWNS;
	}
	else if (parsing_state == READING_RED_SPAWNS)
		parsing_state = READING_BLU_SPAWNS;
	else if (parsing_state == READING_BLU_SPAWNS || parsing_state == READING_SHARED_SPAWNS)
	{
		PrintToChatAll("2");
		parsing_state = END_OF_READING_ARENA;
	}
	else if (parsing_state == END_OF_READING_ARENA)
	{
		PrintToChatAll("3");
		parsing_state = READING_ARENA_NAMES;
	}
	else if (parsing_state == READING_ARENA_NAMES)
	{
		PrintToChatAll("4");
		parsing_state = READING_MAP_NAMES;
	}
	else if (parsing_state == READING_MAP_NAMES)
	{
		PrintToChatAll("5");
		//PrintToChatAll("gay");
		// we're done parsing the config
		
		// write_config();
		//arena_index[map_index]++;
		
		if(workaround_timer_handle != INVALID_HANDLE)
		{
			KillTimer(workaround_timer_handle);
			workaround_timer_handle = INVALID_HANDLE;
		}
		workaround_timer_handle = CreateTimer(0.2, timer_workaround, user);
	}	
}
String:return_option_name(option_index)
{
	decl String:result[128];
	switch(option_index)
	{
		case 0: result = "red_team_size";
		case 1: result = "blu_team_size";
		case 2: result = "separate_spawns";
		case 3: result = "gametype";
		case 4: result = "respawn_delay";
		case 5: result = "health_isnormal";
		case 6: result = "health";
		case 7: result = "unlimited_ammo";
	}
	return result;
}
write_config()
{
	if(DeleteFile(config_path))
	{
		new Handle:newconfig = OpenFile(config_path, "w");
		if (newconfig == INVALID_HANDLE) return INVALID_HANDLE;
		CloseHandle(newconfig);
		
		new Handle:config = OpenFile(config_path, "r+");
		if (config == INVALID_HANDLE) return INVALID_HANDLE;
		
		for (new write_map_index = 0; write_map_index < map_index; write_map_index++)
		{	
			WriteFileLine(config, "\"%s\"", map_name)
			WriteFileLine(config, "{")
			for (new write_arena_index = 0; write_arena_index < arena_index[map_index]; write_arena_index++)
			{
				PrintToChatAll("_%s", arena_names[write_map_index][write_arena_index]);
				WriteFileLine(config, "\t \"%s\"", arena_names[write_map_index][arena_index[write_map_index]])
				//WriteFileLine(config, "\t \"middle\"", arena_names[write_map_index][write_arena_index])
				WriteFileLine(config, "\t{")
				WriteFileLine(config, "\t\t\"options\"")
				WriteFileLine(config, "\t\t{")
				for (new options_index = 0; options_index < Options; options_index++)
				{
					WriteFileLine(config, "\"%s\" \"%s\"", return_option_name(options_index), arena_options[write_map_index][write_arena_index][options_index]);
				}
				WriteFileLine(config,"\t\t}")
				WriteFileLine(config,"\t\t\"spawns\"");
				WriteFileLine(config, "\t\t{")
				
				WriteFileLine(config, "\t\t\t\"red\"")
				WriteFileLine(config, "\t\t\t{")
				decl String: spawn_string[256];
				for (new write_spawn_index = 0; write_spawn_index < new_spawn_amount; write_spawn_index++) // for each saved spawn, input it into RED
				{
					Format(spawn_string, sizeof(spawn_string), "%f %f %f %f %f %f", new_spawn_points[write_spawn_index][RED][POSITION][0],
																		new_spawn_points[write_spawn_index][RED][POSITION][1],
																		new_spawn_points[write_spawn_index][RED][POSITION][2],
																		new_spawn_points[write_spawn_index][RED][EYES][0],
																		new_spawn_points[write_spawn_index][RED][EYES][1],
																		new_spawn_points[write_spawn_index][RED][EYES][2]);
				}
					//WriteFileLine(config, "\t\t\t\"%s\"", spawn_string)
				WriteFileLine(config, "\t\t\t}")
				WriteFileLine(config, "\t\t\t\"blu\"")
				WriteFileLine(config, "\t\t\t{")
				for (new write_spawn_index = 0; write_spawn_index < new_spawn_amount; write_spawn_index++) // for each saved spawn, input it into BLU
				{
					Format(spawn_string, sizeof(spawn_string), "%f %f %f %f %f %f", new_spawn_points[write_spawn_index][BLU][POSITION][0],
																		new_spawn_points[write_spawn_index][BLU][POSITION][1],
																		new_spawn_points[write_spawn_index][BLU][POSITION][2],
																		new_spawn_points[write_spawn_index][BLU][EYES][0],
																		new_spawn_points[write_spawn_index][BLU][EYES][1],
																		new_spawn_points[write_spawn_index][BLU][EYES][2]);
				}
				
				WriteFileLine(config, "\t\t}")
				WriteFileLine(config, "\t}")
				PrintToChatAll("arena: %i", write_arena_index);
			}
			WriteFileLine(config, "}");
			PrintToChatAll("map: %i", write_map_index);
		}
		CloseHandle(config);
	}
}

public OnMapStart()
{
	/*
		OnMapStart() is where we will start global timers, get the map spawns, cache sounds, and stuff like that.
	*/
	GetCurrentMap(map_name, sizeof(map_name));
}



//////////////////////////////////////////////////////////////
////////////////////////////MENUS/////////////////////////////
//////////////////////////////////////////////////////////////
public Action:show_red_player_amount_menu(client, args)
{
	new Handle:menu = CreateMenu(show_red_player_amount_menu_HANDLER);
	
	SetMenuTitle(menu, "Number of players on RED");
	AddMenuItem(menu, "1", "1");
	AddMenuItem(menu, "2", "2");
	AddMenuItem(menu, "3", "3");
	AddMenuItem(menu, "4", "4");
	AddMenuItem(menu, "5", "5");
	AddMenuItem(menu, "6", "6");
	SetMenuExitButton(menu, true);
	DisplayMenu(menu, client, 90);
	return Plugin_Handled;
}
public show_red_player_amount_menu_HANDLER(Handle:menu, MenuAction:action, client, selection)
{
	if (action == MenuAction_Select)
	{
		IntToString(++selection, temp_value, sizeof(temp_value));
		arena_options[map_index][arena_index[map_index]][red_team_size] = temp_value;
		show_blu_player_amount_menu(client, -1);
	}
	else if (action == MenuAction_Cancel)
		CloseHandle(menu);
	else if (action == MenuAction_End)
		CloseHandle(menu);
}
//////////////////////////////////////////////////////////////
public Action:show_blu_player_amount_menu(client, args)
{
	new Handle:menu = CreateMenu(show_blu_player_amount_menu_HANDLER);
	
	SetMenuTitle(menu, "Number of players on BLU");
	AddMenuItem(menu, "1", "1");
	AddMenuItem(menu, "2", "2");
	AddMenuItem(menu, "3", "3");
	AddMenuItem(menu, "4", "4");
	AddMenuItem(menu, "5", "5");
	AddMenuItem(menu, "6", "6");
	SetMenuExitButton(menu, true);
	DisplayMenu(menu, client, 90);
	return Plugin_Handled;
}
public show_blu_player_amount_menu_HANDLER(Handle:menu, MenuAction:action, client, selection)
{
	if (action == MenuAction_Select)
	{
		IntToString(++selection, temp_value, sizeof(temp_value));
		arena_options[map_index][arena_index[map_index]][blu_team_size] = temp_value;
		show_share_spawns_menu(client, -1);
	}
	else if (action == MenuAction_Cancel)
		CloseHandle(menu);
	else if (action == MenuAction_End)
		CloseHandle(menu);
}
//////////////////////////////////////////////////////////////
public Action:show_share_spawns_menu(client, args)
{
	new Handle:menu = CreateMenu(show_share_spawns_menu_HANDLER);
	SetMenuTitle(menu, "Do teams share spawn points?");
	AddMenuItem(menu, "1", "Yes");
	AddMenuItem(menu, "2", "No");
	SetMenuExitButton(menu, true);
	DisplayMenu(menu, client, 90);
	return Plugin_Handled;
}
public show_share_spawns_menu_HANDLER(Handle:menu, MenuAction:action, client, selection)
{
	if (action == MenuAction_Select)
	{
		selection++;
		switch(selection)
		{
			case 1: temp_value = "true";
			case 2: temp_value = "false";
		}
		arena_options[map_index][arena_index[map_index]][red_team_size] = temp_value;
		
		show_gametype_menu(client, -1);
	}
	else if (action == MenuAction_Cancel)
		CloseHandle(menu);
	else if (action == MenuAction_End)
		CloseHandle(menu);
}
//////////////////////////////////////////////////////////////
public Action:show_gametype_menu(client, args)
{
	new Handle:menu = CreateMenu(show_gametype_menu_HANDLER);
	SetMenuTitle(menu, "Select a game type");
	AddMenuItem(menu, "0", "Deathmatch");
	AddMenuItem(menu, "1", "Elimination");
	AddMenuItem(menu, "2", "BBall");
	AddMenuItem(menu, "3", "Ultiduo");
	SetMenuExitButton(menu, true);
	DisplayMenu(menu, client, 90);
	return Plugin_Handled;
}
public show_gametype_menu_HANDLER(Handle:menu, MenuAction:action, client, selection)
{
	if (action == MenuAction_Select)
	{
		IntToString(selection, temp_value, sizeof(temp_value));
		arena_options[map_index][arena_index[map_index]][gametype] = temp_value;
		if(strcmp(temp_value, "1", false) == 0) // 1 == elimination
		{
			show_respawn_time_menu(client, -1);
		}
		
	}
	else if (action == MenuAction_Cancel)
		CloseHandle(menu);
	else if (action == MenuAction_End)
		CloseHandle(menu);
}
//////////////////////////////////////////////////////////////
public Action:show_respawn_time_menu(client, args)
{
	new Handle:menu = CreateMenu(show_respawn_time_menu_HANDLER);
	SetMenuTitle(menu, "Select respawn delay");
	AddMenuItem(menu, "1", "0.0s");
	AddMenuItem(menu, "2", "0.5s");
	AddMenuItem(menu, "3", "1.0s");
	AddMenuItem(menu, "4", "1.5s");
	AddMenuItem(menu, "5", "2.0s");
	AddMenuItem(menu, "6", "2.5s");
	AddMenuItem(menu, "7", "3.0s");
	AddMenuItem(menu, "8", "3.5s");
	AddMenuItem(menu, "9", "4.0s");
	AddMenuItem(menu, "10", "4.5s");
	AddMenuItem(menu, "11", "5.0s");
	AddMenuItem(menu, "12", "5.5s");
	AddMenuItem(menu, "13", "6.0s");
	SetMenuExitButton(menu, true);
	DisplayMenu(menu, client, 90);
	return Plugin_Handled;
}
public show_respawn_time_menu_HANDLER(Handle:menu, MenuAction:action, client, selection)
{
	if (action == MenuAction_Select)
	{
		selection++;
		switch(selection)
		{
			case 1: temp_value = "0.0";
			case 2: temp_value = "0.5";
			case 3: temp_value = "1.0";
			case 4: temp_value = "1.5";
			case 5: temp_value = "2.0";
			case 6: temp_value = "2.5";
			case 7: temp_value = "3.0";
			case 8: temp_value = "3.5";
			case 9: temp_value = "4.0";
			case 10: temp_value = "4.5";
			case 11: temp_value = "5.0";
			case 12: temp_value = "5.5";
			case 13: temp_value = "6.0";
		}
		arena_options[map_index][arena_index[map_index]][respawn_delay] = temp_value;
		
		show_player_health_menu(client, -1);
	}
	else if (action == MenuAction_Cancel)
		CloseHandle(menu);
	else if (action == MenuAction_End)
		CloseHandle(menu);
}
//////////////////////////////////////////////////////////////
public Action:show_player_health_menu(client, args)
{
	new Handle:menu = CreateMenu(show_player_health_menu_HANDLER);
	SetMenuTitle(menu, "Set player health");
	AddMenuItem(menu, "1", "Normal");
	AddMenuItem(menu, "2", "300");
	AddMenuItem(menu, "3", "400");
	AddMenuItem(menu, "4", "500");
	AddMenuItem(menu, "5", "600");
	AddMenuItem(menu, "6", "700");
	AddMenuItem(menu, "7", "800");
	AddMenuItem(menu, "8", "900");
	AddMenuItem(menu, "9", "1000");
	AddMenuItem(menu, "10", "1100");
	AddMenuItem(menu, "11", "1200");
	AddMenuItem(menu, "12", "1300");
	AddMenuItem(menu, "13", "1400");
	SetMenuExitButton(menu, true);
	DisplayMenu(menu, client, 90);
	return Plugin_Handled;
}
public show_player_health_menu_HANDLER(Handle:menu, MenuAction:action, client, selection)
{
	if (action == MenuAction_Select)
	{
		selection++;
		switch(selection)
		{
			case 1: temp_value = "0.0";
			case 2: temp_value = "300.0";
			case 3: temp_value = "400.0";
			case 4: temp_value = "500.0";
			case 5: temp_value = "600.0";
			case 6: temp_value = "700.0";
			case 7: temp_value = "800.0";
			case 8: temp_value = "900.0";
			case 9: temp_value = "1000.0";
			case 10: temp_value = "1100.0";
			case 11: temp_value = "1200.0";
			case 12: temp_value = "1300.0";
			case 13: temp_value = "1400.0";
		}
		arena_options[map_index][arena_index[map_index]][health] = temp_value;
		
		show_ammo_menu(client, -1);
	}
	else if (action == MenuAction_Cancel)
		CloseHandle(menu);
	else if (action == MenuAction_End)
		CloseHandle(menu);
}
//////////////////////////////////////////////////////////////
public Action:show_ammo_menu(client, args)
{
	new Handle:menu = CreateMenu(show_ammo_menu_HANDLER);
	SetMenuTitle(menu, "Unlimited Ammo");
	AddMenuItem(menu, "1", "Yes");
	AddMenuItem(menu, "2", "No");
	SetMenuExitButton(menu, true);
	DisplayMenu(menu, client, 90);
	return Plugin_Handled;
}
public show_ammo_menu_HANDLER(Handle:menu, MenuAction:action, client, selection)
{
	if (action == MenuAction_Select)
	{
		selection++;
		switch(selection)
		{
			case 1: temp_value = "true";
			case 2: temp_value = "false";
		}
		arena_options[map_index][arena_index[map_index]][unlimited_ammo] = temp_value;
		//write_config();
	}
	else if (action == MenuAction_Cancel)
		CloseHandle(menu);
	else if (action == MenuAction_End)
		CloseHandle(menu);
}
//////////////////////////////////////////////////////////////
public Action:show_create_spawn_menu(client, args)
{
	new Handle:menu = CreateMenu(show_create_spawn_menu_HANDLER);
	SetMenuTitle(menu, "Spawn Point Management");
	AddMenuItem(menu, "1", "Show Current Spawn Points");
	// AddMenuItem(menu, "2", "Cycle Current Spawn Points");
	AddMenuItem(menu, "2", "");
	AddMenuItem(menu, "3", "Add Current Location");
	SetMenuExitButton(menu, true);
	DisplayMenu(menu, client, 90);
	return Plugin_Handled;
}
public show_create_spawn_menu_HANDLER(Handle:menu, MenuAction:action, client, selection)
{
	if (action == MenuAction_Select)
	{
		switch(++selection)
		{
			case 1: show_spawn_points(client);
			case 2: 
			{
				show_spawn_cycle_menu(client, -1);
				cycle_current_spawn = 0;
			}
			case 3: show_spawn_team_menu(client, -1);
		}
	}
	else if (action == MenuAction_Cancel)
		CloseHandle(menu);
	else if (action == MenuAction_End)
		CloseHandle(menu);
}
//////////////////////////////////////////////////////////////
public Action:show_spawn_cycle_menu(client, args)
{
	new Handle:menu = CreateMenu(show_spawn_cycle_menu_HANDLER);
	SetMenuTitle(menu, "Spawn Point Management");
	if(cycle_current_spawn+1 != new_spawn_amount) // not at the end
		AddMenuItem(menu, "1", "Next Spawn Point");
	else
		AddMenuItem(menu, "1", "No Spawns Left")
	if(cycle_current_spawn != 0) // at the beginning
		AddMenuItem(menu, "2", "Back To Start");
	//else
	//	AddMenuItem(menu, "2", "Previous Spawn Point");
	AddMenuItem(menu, "3", "");
	AddMenuItem(menu, "4", "Back");
	SetMenuExitButton(menu, true);
	DisplayMenu(menu, client, 90);
	return Plugin_Handled;
}
public show_spawn_cycle_menu_HANDLER(Handle:menu, MenuAction:action, client, selection)
{
	if (action == MenuAction_Select)
	{
		switch(++selection)
		{
			case 1:
			{
				if(cycle_current_spawn+1 != new_spawn_amount) // not at the end
				{
					if(!first_cycle)
						cycle_current_spawn++;
					first_cycle = false;
					
					// PrintToChatAll("%f && %f", new_spawn_points[cycle_current_spawn][POSITION][0], new_spawn_points[cycle_current_spawn][POSITION][1]);
					// PrintToChatAll("%i & %i", cycle_current_spawn, new_spawn_amount);
					
					if(teleport_handle != INVALID_HANDLE)
					{
						KillTimer(teleport_handle);
						teleport_handle = INVALID_HANDLE;
					}
					teleport_handle = CreateTimer(0.25, timer_teleport, cycle_current_spawn);
				}
			}
			case 2: 
			{
				/*if(cycle_current_spawn != 0) // not beginning
				{
					first_cycle = true;
					cycle_current_spawn--;
					if(teleport_handle != INVALID_HANDLE)
					{
						KillTimer(teleport_handle);
						teleport_handle = INVALID_HANDLE;
					}
					teleport_handle = CreateTimer(0.25, timer_teleport, cycle_current_spawn);
				}*/
				cycle_current_spawn = 0;
				first_cycle = false;
				if(teleport_handle != INVALID_HANDLE)
				{
					KillTimer(teleport_handle);
					teleport_handle = INVALID_HANDLE;
				}
				teleport_handle = CreateTimer(0.25, timer_teleport, cycle_current_spawn);
			}
			case 4: show_create_spawn_menu(client, -1);
		}
		if(selection != 6)
			show_spawn_cycle_menu(client, -1); // reopen the menu
	}
	else if (action == MenuAction_Cancel)
		CloseHandle(menu);
	else if (action == MenuAction_End)
		CloseHandle(menu);
}
//////////////////////////////////////////////////////////////
public Action:show_spawn_team_menu(client, args)
{
	new Handle:menu = CreateMenu(show_spawn_team_menu_HANDLER);
	SetMenuTitle(menu, "For which team?");
	AddMenuItem(menu, "1", "Red");
	AddMenuItem(menu, "2", "Blu");

	SetMenuExitButton(menu, true);
	DisplayMenu(menu, client, 90);
	return Plugin_Handled;
}
public show_spawn_team_menu_HANDLER(Handle:menu, MenuAction:action, client, selection)
{
	if (action == MenuAction_Select)
	{
		switch(++selection)
		{
			case 1:
			{
				add_current_location(client, RED)
			}
			case 2: 
			{
				add_current_location(client, BLU)
			}
		}
	}
	else if (action == MenuAction_Cancel)
		CloseHandle(menu);
	else if (action == MenuAction_End)
		CloseHandle(menu);
}
//////////////////////////////////////////////////////////////

